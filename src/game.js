Game = {
	
	victoryText: "spr_victory",
	
	deathText: "spr_death",
	
	confessionText: "spr_confession",
	
	friendshipText: "spr_friendship",
	
	scale: 2,
	
	time: "day",
	
	sheepCount: 0,
	
	peopleCount: 0,
	
	playerHouse: [[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[3,1],[3,0],[3,0],[3,0],[3,0],[3,0],[3,0],[1,2],[1,2],[3,2],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[3,1],[3,0],[3,0],[3,0],[3,0],[2,1],[2,1,true],[3,0],[3,0],[3,2],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[1,1],[1,1],[1,1],[0,1],[0,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[1,1],[1,1],[1,1],[0,1],[0,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[1,1],[1,1],[1,1],[0,1],[0,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]],
		  [[1,1],[1,1],[1,1],[1,1],[0,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]]],
	
	playerRoof: [[-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
		  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
		  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
		  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
		  [-1,-1,-1,-1,-1,-1,-1,-1,[2,2],[2,2],-1,-1,-1,-1,-1],
		  [-1,[4,3],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1],[4,1],[4,3,true],-1,-1,-1,-1],
		  [-1,[4,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[4,2,true],-1,-1,-1,-1],
		  [-1,[4,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[4,2,true],-1,-1,-1,-1],
		  [-1,[4,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[4,2,true],-1,-1,-1,-1],
		  [-1,[4,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[4,2,true],-1,-1,-1,-1],
		  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
		  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
		  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
		  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
		  [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]],
	
	church: [[[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[1,1]],
			 [[1,1],[1,1],[3,1],[1,2],[1,2],[3,0],[3,0],[3,0],[3,0],[3,0],[3,0],[1,2],[1,2],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1]],
			 [[1,1],[1,1],[3,1],[3,0],[3,0],[3,0],[2,0],[2,0],[2,0],[2,0],[3,0],[3,0],[3,0],[3,2],[1,1]],
			 [[1,1],[1,1],[1,1],[1,1],[1,1],[3,1],[2,0],[2,0],[2,0],[2,0],[3,2],[1,1],[1,1],[1,1],[1,1]],
			 [[1,1],[1,1],[1,1],[1,1],[1,1],[3,1],[3,0],[2,1],[2,1,true],[3,0],[3,2],[1,1],[1,1],[1,1],[1,1]],
			 [[1,1],[1,1],[1,1],[1,1],[1,1],[1,1],[2,3],[1,1],[1,1],[2,3],[1,1],[1,1],[1,1],[1,1],[1,1]]],
	
	churchRoof: [[-1,-1,-1,[2,2],[2,2],-1,-1,-1,-1,-1,-1,[2,2],[2,2],-1,-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],[2,2],-1],
			 [-1,-1,-1,-1,-1,-1,[2,2],[2,2],[2,2],[2,2],-1,-1,-1,-1,-1],
			 [-1,-1,-1,-1,-1,-1,[2,2],[2,2],[2,2],[2,2],-1,-1,-1,-1,-1],
			 [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1],
			 [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]],
	
	
	
	tileReqs: [["Tile","Tile","Tile","SolidTile","Tile"],
			   ["SolidTile","Tile","Door","SolidTile","Tile"],
			   ["","Window","Tile","SolidTile","Tile"],
			   ["Tree", "", "Torch", "SolidTile","Tile"],
			   ["Fence","Fence","Fence","Fence"]],
	
	keybindings: {
		up: "UP_ARROW",
		upALT: "W",
		down: "DOWN_ARROW",
		downALT: "S",
		left: "LEFT_ARROW",
		leftALT: "A",
		right: "RIGHT_ARROW",
		rightALT: "D"
	},
	
	// The total width of the game screen. 
	width: function() {
		return Crafty.DOM.window.width;
	},
 
	// The total height of the game screen. 
	height: function() {
		return Crafty.DOM.window.height;
	},
	
	mapGenerationItems: ["house","farm","lake","forest","field", "church", "playerHouse"],
	mapGenerationProbabilities: [.05,.05,.15,.2,.15,.2,.2],
	
	generateMap: function (w,h,zonew,zoneh){
		var map = [];
		var rooves = [];
		for (var y = 0; y < h; y++) {
			map.push([]);
			rooves.push([]);
			for (var x = 0; x < w; x++) {
				map[y].push([0,0]);
				rooves[y].push(-1);
			}
		}
		var zones = [];
		for (y = 0; y < h/zoneh; y++){
			zones.push([]);
			for (x = 0; x < w/zonew; x++) {
				// could do roulette search
				var rand = Math.random();
				for (var i = 0; i < this.mapGenerationProbabilities.length; i++){
					if (rand < this.mapGenerationProbabilities[i]){
						zones[y].push(this.mapGenerationItems[i])
						// Generate that area's sprites
						var area = this.generateZone(this.mapGenerationItems[i],zonew,zoneh);
						// Only have one church and player house
						if (this.mapGenerationItems[i] === "church" ||
						    this.mapGenerationItems[i] === "playerHouse"){
							this.mapGenerationProbabilities[i] -= .2;
							this.mapGenerationProbabilities[1] += .1;
							this.mapGenerationProbabilities[0] += .05;
							this.mapGenerationProbabilities[2] += .05;
							if (this.mapGenerationItems[i] === "playerHouse"){
								Game.playerPos = [(x*zonew*12*Game.scale)+180,(y*zoneh*12*Game.scale)+180];	
							}
							if (this.mapGenerationItems[i] === "church"){
								Game.priestPos = [(x*zonew*12*Game.scale)+180,(y*zoneh*12*Game.scale)+180];	
							}
						}
						// Assign that area to the appropriate part of the map
						for (var areay = 0; areay < area[0].length; areay++){
							for (var areax = 0; areax < area[0][areay].length; areax++){
								map[(y*zoneh)+areay][(x*zonew)+areax] = area[0][areay][areax];
								// If the area has a roof, define the rooves for that area
								if (area[1]){
									rooves[(y*zoneh)+areay][(x*zonew)+areax] = area[1][areay][areax];	
								}
							}
						}
						break;
					} else {
						rand -= this.mapGenerationProbabilities[i];	
					}
				}
			}
		}
		
		return [map,rooves];
			
	},
	
	generateZone: function(zoneName,zonew,zoneh){
		var area = [];
		area.push([]);
		area.push(false);
		for (y = 0; y < zoneh; y++){
			area[0].push([]);
			for (x = 0; x < zonew; x++){
				area[0][y].push([1,1]);	
			}
		}
		// Generate Houses
		if (zoneName === "house"){
			// Define the roof's defaults
			area[1] = [];
			for (y = 0; y < zoneh; y++){
				area[1].push([]);
				for (x = 0; x < zonew; x++){
					area[1][y].push(-1);	
				}
			}
			
			var x, y, w, h, doorCount, windowCount;
			//get an x, y, w, and h for the house
			// Keep the top corner in the top corner of the zone
			x = Math.floor(Math.random()*zonew/2);
			y = Math.max(Math.floor(Math.random()*zoneh/2),1);
			// Keep a minimum width and height for the house
			w = Math.floor(Math.random()*((zonew-x)-4))+4;
			h = Math.floor(Math.random()*((zoneh-y)-4))+4;
			// Generate window numbers and door numbers
			doorCount = 1;
			windowCount = Math.floor(Math.random()*3);
			peopleCount = 1;
			for (var housey = 0; housey < h; housey++){
				for (var housex = 0; housex < w; housex++){
					if (housey !== h-1){
						area[1][housey+y][housex+x] = [2,2];
					}
					if (housex === 0){
						area[0][housey+y][housex+x] = [3,1];
						if (housey === 0){
							area[1][housey+y][housex+x] = [4,3];
						} else if (housey !== h-1){
							area[1][housey+y][housex+x] = [4,2];
						}
					} else if (housex === w-1) {
						area[0][housey+y][housex+x] = [3,2];
						if (housey === 0) {
							area[1][housey+y][housex+x] = [4,3,true];
						} else if (housey !== h-1){
							area[1][housey+y][housex+x] = [4,2,true];
						}
					} else if (housey === 0 || housey === h-1) {
						area[0][housey+y][housex+x] = [3,0];
						if (housey === 0){
							area[1][housey+y][housex+x] = [4,1];
						}
						if (housey === 0){
							// Place a window?
							windowBool = (Math.random() < (1*windowCount*(housex/(w-2))));
							if (windowBool) {
								area[0][housey+y][housex+x] = [1,2];
								area[0][housey+y][housex+x-1] = [1,2];
								area[1][housey+y-1][housex+x] = [2,2];
								area[1][housey+y-1][housex+x-1] = [2,2];
								windowCount--;
							}
						} else if (housey === h-1 && housex !== 1){
							// Place a door?
							doorBool = (Math.random() < (1*doorCount*(housex/(w-2))));
							if (doorBool && doorCount > 0) {
								area[0][housey+y][housex+x] = [2,1,true];
								area[0][housey+y][housex+x-1] = [2,1];
								if (housey+y !== zoneh) {
									area[0][housey+y+1][housex+x-2] = [2,3];	
									if (peopleCount !== 0){
										area[0][housey+y+1][housex+x].push("Person");
										peopleCount--;
									}
								}
								doorCount--;
							}
						}
					} else {
						area[0][housey+y][housex+x] = [2,0];
					}
				}
			}
		// generate lakes
		} else if (zoneName === "lake") {
			//TODO: add circular lakes
			var x, y, w, h, edgeTilesRemoved;
			x = Math.floor(Math.random()*zonew/2);
			y = Math.floor(Math.random()*zoneh/2);
			// Keep a minimum width and height for the lake
			w = Math.max(Math.floor(Math.random()*zonew-x),5);
			h = Math.max(Math.floor(Math.random()*zoneh-y),5);
			// Generate number of edge tiles to take out
			edgeTilesRemoved = Math.floor(Math.random()*((2*w)+(2*h)));
			for (var lakey = 0; lakey < h; lakey++){
				for (var lakex = 0; lakex < w; lakex++){
					area[0][lakey+y][lakex+x] = [0,1];
					if (lakey === 0 || lakey === h-1 || lakex === 0 || lakex === w-1){ 
						// Remove an edge?
						edgeBool = (Math.random() < (1*edgeTilesRemoved*((x+y)/(w-1+h-1))));
						if (edgeBool) {
							area[0][lakey+y][lakex+x] = [0,0];
							edgeTilesRemoved--;
						}
					}
				}
			}
		// generate forests
		} else if (zoneName === "forest"){
			var density = Math.min(Math.max(Math.random(),.05),.35);
			for (var foresty = 0; foresty < zoneh; foresty++){
				for (var forestx = 0; forestx < zonew; forestx++){
					treeHere = (Math.random() < density)
					if (treeHere){
						area[0][forestx][foresty] = [0,3];	
					} else if (Math.random() < .25) {
						area[0][forestx][foresty] = [1,1];
					} else {
						area[0][forestx][foresty] = [0,0];	
					}
				}
			}
		// The church building (unique)
		} else if (zoneName === "church"){
			area[0] = Game.church;
			area[1] = Game.churchRoof;
		// The player's house (unique)
		} else if (zoneName === "playerHouse"){
			area[0] = Game.playerHouse;
			area[1] = Game.playerRoof;
		// generate fields and farms
		} else {
			for (var fieldy = 0; fieldy < zoneh; fieldy++){
				for (var fieldx = 0; fieldx < zonew; fieldx++){
					if (fieldx === 0 || fieldx === zonew-1 || fieldy === 0 || fieldy === zoneh-1){
						area[0][fieldy][fieldx] = [1,1];
					} else if ((fieldx === 1 || fieldx === zonew-2
							   || fieldy === 1 || fieldy === zoneh-2)){
						// Fences
						if (fieldx === 1 && fieldy === zoneh-2){
							area[0][fieldy][fieldx] = [1,4];
						} else if (fieldx === 1 && fieldy === 1) {
							area[0][fieldy][fieldx] = [3,4];	
						} else if (fieldx === zonew-2 && fieldy === 1) {
							area[0][fieldy][fieldx] = [3,4,true];	
						} else if (fieldx === zonew-2 && fieldy === zonew-2) {
							area[0][fieldy][fieldx] = [1,4,true];	
						} else if (fieldx === 1) {
							area[0][fieldy][fieldx] = [2,4];	
						} else if (fieldx === zonew-2) {
							area[0][fieldy][fieldx] = [2,4,true];	
						} else {
							var flip = (Math.random() < .5);
							area[0][fieldy][fieldx] = [0,4,flip]; 	
						}
					} else {
						area[0][fieldy][fieldx] = [1,0];
						if (zoneName === "farm" && Math.random() < .5) {
							area[0][fieldy][fieldx] = [3,3];	
						} else if (zoneName === "field" && Math.random() < .025) {
							area[0][fieldy][fieldx].push('Sheep');
							Game.sheepCount++;
						}
					}

				}
			}
		}
		return area;
	},
	
	drawMap: function(grid, initx, inity, extraComponent,globalZ) {
		var sheet = "spr_tileSheetBig,";
		var tile;
		var popIndex = 0;
		// Wrap the world in water
		if (!extraComponent){
			for (var y = -1; y <= grid.length; y+=1){
				for (var x = -1; x <= grid[0].length; x+= 1){
					if (x === -1 || x === grid[0].length || y === -1 || y === grid.length){
						tile = Crafty.e(sheet + this.tileReqs[1][0]);
						tile.sprite(0,1);
						tile.attr({x:initx + (x*12*this.scale),y:inity + (y*12*this.scale)});
						tile.sheetx = 0;
						tile.sheety = 1;
						tile._globalZ = globalZ;
					}
				}
			}
		}
		for (y = 0; y < grid.length; y++) {
			for (x = 0; x < grid[y].length; x++) {
				if (grid[y][x] === -1){
					continue;	
				}
				
				tile = Crafty.e(sheet + extraComponent + ", "
								+ this.tileReqs[grid[y][x][1]][grid[y][x][0]]);
				tile.sprite(grid[y][x][0],grid[y][x][1]);
				tile.attr({x:initx + (12*this.scale*x),y:inity + (12*this.scale*y)});
				if (grid[y][x][2]){
					if (typeof grid[y][x][2] === "string"){
						if (grid[y][x][2] === "Person") {
							Game.people++;	
							Crafty.e(this.pop.newPeople[popIndex]).attr({x:initx + (12*this.scale*x),
																		 y:inity + (12*this.scale*y)});
							popIndex = (popIndex + 1) % 4;
						} else {
							Crafty.e(grid[y][x][2]).attr({x:initx + (12*this.scale*x),y:inity + (12*this.scale*y)});
						}
					} else {
						tile._flipX = true;
					}
				}
				tile.sheetx = grid[y][x][0];
				tile.sheety = grid[y][x][1];
				tile._globalZ = globalZ;
			}
		}	
	},
	
	viewportmax: {
		x:0,
		y:0
	},
	
	viewportCheck: function(){
		oldx = Crafty.viewport._x;
		oldy = Crafty.viewport._y;
		if (Crafty.viewport._x > 0){
			Crafty.viewport.x = 0;	
		} else if (Crafty.viewport._x < -(Game.viewportmax.x)+Game.width()){
			Crafty.viewport.x = -(Game.viewportmax.x)+Game.width();
		}
		if (Crafty.viewport._y > 0){
			Crafty.viewport.y = 0;	
		} else if (Crafty.viewport._y < -(Game.viewportmax.y)+Game.height()){
			Crafty.viewport.y = -(Game.viewportmax.y)+Game.height();
		}
		if (oldx !== Crafty.viewport._x){
			Crafty.trigger('MoveUI', ["x",oldx - Crafty.viewport._x]);	
		}
		if (oldy !== Crafty.viewport._y){
			Crafty.trigger('MoveUI', ["y",oldy - Crafty.viewport._y]);	
		}
		 	
	},
	
	people: 0,
	
	_uiGlobalZ: 5,
	
	uiGlobalZ: function (){
		this._uiGlobalZI++;
		return this._uiGlobalZ;
	},
	
	start: function() {
		// Start crafty and set a background color so that we can see it's working
		Crafty._pixelartEnabled = true;
		Crafty.init(Game.width(), Game.height());
		Crafty.background('rgb(0, 0, 0)');
		Crafty.viewport.clampToEntities = true;
		Crafty.scene('Load');
	}
}

Crafty.scene('Load', function() {
	// Loading stuff
	
	Crafty.load([], function(){
		
		Crafty.sprite(24,24, 'assets/peoplebig.png', {
			spr_peopleBig: [0,0]
		}, 4, 4);
		Crafty.sprite(24,24, 'assets/tilesbig.png', {
			spr_tileSheetBig: [0,0]
		}, 4, 4);
		Crafty.sprite(636,20, 'assets/textbutton.png', {
			spr_textButton: [0,0]
		}, 2, 2);
		Crafty.sprite(480,120, 'assets/UI.png', {
			spr_ui: [0,0]
		}, 2, 2);
		Crafty.sprite(640,480, 'assets/opening.png', {
			spr_opening: [0,0]
		}, 2, 2);
		Crafty.sprite(640,480, 'assets/confessEnding.png', {
			spr_confession: [0,0]
		}, 2, 2);
		Crafty.sprite(640,480, 'assets/friendEnding.png', {
			spr_friendship: [0,0]
		}, 2, 2);
		Crafty.sprite(640,480, 'assets/deathEnding.png', {
			spr_death: [0,0]
		}, 2, 2);
		Crafty.sprite(640,480, 'assets/victoryEnding.png', {
			spr_victory: [0,0]
		}, 2, 2);
		Crafty.sprite(640,74, 'assets/werewolf.png', {
			spr_werewolfText: [0,0]
		}, 2, 2);
		Crafty.sprite(248,54, 'assets/startbutton.png', {
			spr_startButton: [0,0]
		}, 2, 2);
		Crafty.sprite(232,54, 'assets/startOverbutton.png', {
			spr_continueButton: [0,0]
		}, 2, 2);
		Crafty.sprite(640,360, 'assets/dialoguebox.png', {
			spr_dialogueBox: [0,0]
		}, 2, 2);

		Crafty.audio.add("aud_night","assets/music/music1.mp3");
		Crafty.audio.add("aud_day","assets/music/music2.mp3");
		Crafty.audio.add("sfx_baa","assets/sfx/Baa.wav");
		Crafty.audio.add("sfx_bite","assets/sfx/Bite.wav");
		Crafty.audio.add("sfx_select","assets/sfx/Blip_Select.wav");
		Crafty.audio.add("sfx_bonk","assets/sfx/Bonk.wav");
		Crafty.audio.add("sfx_gunshot","assets/sfx/Explosion.wav");
		Crafty.audio.add("sfx_hurt","assets/sfx/Hit_Hurt.wav");
		Crafty.audio.add("sfx_yell","assets/sfx/Yell.wav");
		Crafty.audio.add("sfx_glass","assets/sfx/Glass.wav");
		
		Crafty.scene('Game');
	});
});

Crafty.scene("Game", function () {
	Game.mapx = 150;
	Game.mapy = 150;
	generatedMap = Game.generateMap(Game.mapx,Game.mapy,15,15);
	Game.viewportmax.x = Game.mapx*Game.scale*12;
	Game.viewportmax.y = (Game.mapy*Game.scale*12) + 118;
	var werewolfText;
	
	Crafty.bind("Intro", function() {
		
		//Initalize the population of civilians in town
		Game.pop = Crafty.e("Population");
		
		Crafty.audio.play("aud_day",-1, .25);
		//Clear up things from old game cycles
		Crafty("*").destroy();
		
		//Intro screen 
		var background = Crafty.e('2D,Canvas,spr_opening,UI, PreGame');
		background.setCenteredPos(-320,-240,1,1);
		var startButton = Crafty.e('StartButton, PreGame');
		startButton.setCenteredPos(40,100,1,1);
		
		// Figure out why this doesn't work
		// It is supposed to come up with a rectangle filled with smaller rectangles
		// of various sizes but keeps looping forever before finishing
		//zoneSizes = Game.generateZoneSizes(10,10);
		werewolfText = Crafty.e('2D,Canvas,UI,spr_werewolfText, Delay, PreGame');
		werewolfText.setCenteredPos(-320,166,1,1);
		werewolfText.sprite(0,1);
	});
	
	Crafty.bind('StartGamePrep', function() {
		werewolfText.sprite(0,0);
		werewolfText.delay(function () {Crafty('PreGame').destroy(); Crafty.trigger('GameStart');},100);
	});
	
	Crafty.bind('GameStart', function() {
		
		Game.pop.threat = 0;
		
		//Make the map
		Game.drawMap(generatedMap[0],0,0,"",1);
		//Make the rooves to buildings
		Game.drawMap(generatedMap[1],0,0,"Roof",3);

		//Make the player
		var player = Crafty.e("Player").attr({x:Game.playerPos[0],y:Game.playerPos[1]});
		
		//Make the priest
		var priest = Crafty.e("Priest").attr({x:Game.priestPos[0],y:Game.priestPos[1]});
		Game.people++;
		Game.totalPeople = Game.people;
		
		//Make the UI
		Crafty.e('spr_ui, UI').setCenteredPos(-240, -120, 1, 2);
		weirdThingThatLetsOtherThingsHaveColor = Crafty.e('Actor, Color').attr({x:5,y:5})
			  .color("white");
		Crafty.e("Timer").setCenteredPos(-25,-90,1,2);
		peopleLeft = Crafty.e("Actor, Text, UI");
		peopleLeft.setCenteredPos(-108, -30, 1, 2);
		peopleLeft.textFont({family: 'Verdana', size: '18px'});
		peopleLeft.textColor('white');
		peopleLeft.text("Remaining Civilians: " + Game.people.toString());
		
		peopleLeft.bind('CivilianDeath', function() {
			Game.people--;
			this.text("Remaining Civilians: " + Game.people.toString());
			if (Game.people === 0){
				Crafty.trigger('GameEnd', Game.victoryText);	
			}
			Game.pop.threat = 120/Game.people;
			if (Game.pop.threat < 30){
				Game.pop.threat = 0;	
			} else if (Game.pop.threat < 60) {
				Game.pop.threat = 30;	
			} else if (Game.pop.threat < 100) {
				Game.pop.threat = 60;
			} else {
				Game.pop.threat = 100;	
			}
		});
		
		Crafty.bind("MoveUI", Game.viewportCheck);
	});
	
	// Disable screen-moving buttons default operations
	Crafty.bind('KeyDown', function(data){
		bannedKeys = [32,37,38,39,40];
		if(bannedKeys.indexOf(data.keyCode) !== -1) {
			data.preventDefault();	
		}
	});

	Crafty.bind('NormalTime', function() {
		Crafty.audio.stop("aud_night");
		Crafty.audio.play("aud_day", -1, .25);
	});
	
	Crafty.bind('MonsterTime', function() {
		Crafty.audio.stop("aud_day");
		Crafty.audio.play("aud_night", -1, .25);
	});
	
	Crafty.bind('GameEnd', function(text) {
		Crafty.audio.stop();
		Crafty('*').destroy();
		ending = Crafty.e('UI, '+text);
		ending.setCenteredPos(-320,-240,1,1);
		ending.alpha = 0;
		ending._globalZ = 99;
		ending.bind('EnterFrame', function() {
			if (!this.continueButton) {
				this.continueButton = Crafty.e('UI, Button, spr_continueButton');
				this.continueButton._globalZ = 100;
				this.continueButton.setCenteredPos(90,190,1,1);
				this.continueButton.bind('Click', function () {
					Crafty.trigger('Intro');
				});
				this.continueButton.alpha = 0;
			}
			if (ending._alpha !== 1){
				if (this.continueButton._alpha < .75){
					this.continueButton.alpha = this.continueButton._alpha + .01;	
				}
				ending.alpha = ending._alpha + .01;
			}  
		});
	});
	Crafty.trigger('Intro');

});

Crafty.c('Population', {
	init: function () {
	},
	
	threat: 0,
	
	// Used, in order, for making new people
	newPeople: ["Gunner","Friend","Yeller","Runner"],
	
	dialogue: {
		//yPos
		1: {
			//suspicion
			0: {
				//Things that can be said to a person
				hello: "Howdy!",
				goodbye: "See ya!",
				deny: "Buddy, I'd never suspect you!",
				worry: "What's there to worry about?"
			},
			30: {
				hello: "Hey.",
				goodbye: "Goodbye!",
				deny: "Ok. Don't worry, I believe you.",
				worry: "Alright, I'll stay safe."
			},
			60: {
				hello: "Oh. What do you want?",
				goodbye: "Right.",
				deny: "Prove it, then come back to me.",
				worry: "I don't need to hear that from you."
			},
			100: {
				hello: "Stay away from me!",
				goodbye: "Go find someone else to bother!",
				deny: "Yeah right!",
				worry: "What?!"
			}
		},
		2: {
			//suspicion
			0: {
				//Things that can be said to a person
				hello: "Good day!",
				goodbye: "See you soon!",
				deny: "Well, of course, it couldn't be you.",
				worry: "Thanks."
			},
			30: {
				hello: "Uh, hey there.",
				goodbye: "Talk to you later!",
				deny: "Sure.",
				worry: "Yeah."
			},
			60: {
				hello: "What are you doing here?",
				goodbye: "Sure.",
				deny: "I'm... not so sure. Look, I should go.",
				worry: "I'd worry about yourself."
			},
			100: {
				hello: "It's the monster! Someone help!",
				goodbye: "Help me!",
				deny: "Stay away from me!",
				worry: "I'm not listening! Leave me alone!"
			}
		},
		3: {
			//suspicion
			0: {
				//Things that can be said to a person
				hello: "Ahoy there! Out to help on the field?",
				goodbye: "Hey, come back tomorrow?",
				deny: "Well of course not!",
				worry: "Well, hopefully nothing happens."
			},
			30: {
				hello: "The town is heating up with all this violence, but, you and me, right?",
				goodbye: "Thanks for the chat!",
				deny: "Of course not, man.",
				worry: "Thank you for your thoughts."
			},
			60: {
				hello: "Hey, friend. How's it going?",
				goodbye: "So long. Don't worry about all this nonsense. I'll still be around for you, buddy.",
				deny: "I won't believe them all, don't you worry.",
				worry: "Hey, I'm worried about you too."
			},
			100: {
				hello: ". . . I know what they've said, and it hurts me but I can't trust you anymore.",
				goodbye: "I'm so sorry . . .",
				deny: "I . . . really?",
				worry: "Why are you worried? What are you going to do?",
				ending1: ". . . I trust you. I know you wouldn't hurt me.",
				ending2: "Alright. I'll grab some things, and then let's get out of here."
			}
		},
		4: {
			//suspicion
			0: {
				//Things that can be said to a person
				hello: "How ya doin'?",
				goodbye: "Farewell!",
				deny: "Pretty hasty to deny that ain't ya?",
				worry: "You too.",
				threaten: "I'm not gonna fight ya, mate. It wouldn't be close to fair."
			},
			30: {
				hello: "Hey there.",
				goodbye: "Bye.",
				deny: "It's not my problem, whether you is or isn't it.",
				worry: "No worries, mate.",
				threaten: "Come on now, do you have a deathwish?"
			},
			60: {
				hello: "Back off.",
				goodbye: "Shove off.",
				deny: "Oh yeah? Come back here at midnight and show me.",
				worry: "Don't gotta worry about me.",
				threaten: "Was just looking for an excuse."
			},
			100: {
				hello: "No time for talking, beast! Put up your guns!",
				goodbye: "Won't be seeing you after this!",
				deny: "Hah!",
				worry: "You should be worrying about your own hide.",
				threaten: "Not beating me to that, you bastard!"
			}
		}, 
		5: {
			//suspicion
			0: {
				//Things that can be said to a person
				hello: "Good day, my child. May God bless you on this day.",
				goodbye: "Until next time we meet, child.",
				deny: "Well, yes, my child.",
				worry: "We'll all be alright, child.",
				confess: "What? That's ridiculous, but if you are what you say, God's light will purify you."
			},
			30: {
				hello: "Hello there, my child. What brings you here?",
				goodbye: "Until tomorrow, child.",
				deny: "I wouldn't have guessed as much.",
				worry: "Thank you for your kind words my child.",
				confess: "Well, I had heard some things but . . . . Stand still, and you will be purified."
			},
			60: {
				hello: "My son, There have been rumors about. If there's something you've done, you can always confess.",
				goodbye: "Farewell, my son.",
				deny: "You still seem troubled. Is there anything you need to say?",
				worry: "I'm safe, but thank you.",
				confess: "As I had thought. Don't move, this will hurt more if you do."
			},
			100: {
				hello: "Ah. Have you come to confess? You can still find some peace in God, despite your terrible crimes.",
				goodbye: "Wait just a moment, child.",
				deny: "Please, confess.",
				worry: "There's no need for that now.",
				confess: "Beg for God's mercy, my son."
			}
		}
	}
});

Crafty.c('Actor', {
	init: function() {
		this.requires('2D, Canvas, Collision');	
	}
});

Crafty.c('SmartCollision', {
	init: function() {
		this.requires('Actor');
	},
	
	sCollide: function(component) {
		var hits = this.hit(component);	
		if (!hits) {
			return false;	
		}
		toReturn = false;
		hits.some(function (data) {
			if (data.obj.has(component)){
				toReturn = data.obj;
				return true;
			}
		});
		return toReturn;
	}
	
});

Crafty.c('MovingActor', {
	init: function () {
		this.requires('SmartCollision');
		this._globalZ = 2;
		this.xSpeed = 3*Game.scale;
		this.ySpeed = 3*Game.scale;
	},
	
	_actormove: function () {
		if (this._movement.x || this._movement.y){
			if (this._movement.x !== 0){
				this._flipX = (this._movement.x < 0);
			}
			this.x = this._x + this._movement.x * this.xSpeed;
			this.y = this._y + this._movement.y * this.ySpeed;

			var collided = this.sCollide('Solid');
			if (collided && !collided.has("Hostile")) {
				this.stopMoving();	
			}
		}
	},
	
	stopMoving: function () {
		this.x -= this._movement.x * this.xSpeed;
		this.y -= this._movement.y * this.ySpeed;
	}
});

Crafty.c('PlayerActor', {
	init: function() {
		this.requires('Gamepad, Keyboard, MovingActor');
		this.bind('KeyUp', this.getMovementInput);
		this.bind('KeyDown', this.getMovementInput);
	},
	
	getMovementInput: function (){
		if (this.upPressed()){
			if (this.leftPressed()){
				this._movement.x = -.5;
				this._movement.y = -.5;
			} else if (this.rightPressed()){
				this._movement.x = .5;
				this._movement.y = -.5;
			} else {
				this._movement.x = 0;
				this._movement.y = -1;
			}
		} else if (this.downPressed()) {
			if (this.leftPressed()){
				this._movement.x = -.5;
				this._movement.y = .5;
			} else if (this.rightPressed()){
				this._movement.x = .5;
				this._movement.y = .5;
			} else {
				this._movement.x = 0;
				this._movement.y = 1;
			}
		} else if (this.leftPressed()) {
			this._movement.y = 0;
			this._movement.x = -1;
		} else if (this.rightPressed()) {
			this._movement.y = 0;
			this._movement.x = 1;
		} else {
			this._movement.x = 0;
			this._movement.y = 0;
			this.noMovement = true;	
		}
	},
	
	upPressed: function (){
		return this.isDown(Game.keybindings['up']) || this.isDown(Game.keybindings['upALT']);	
	},
	
	leftPressed: function (){
		return this.isDown(Game.keybindings['left']) || this.isDown(Game.keybindings['leftALT'])
	},
	
	rightPressed: function (){
		return this.isDown(Game.keybindings['right']) || this.isDown(Game.keybindings['rightALT'])
	},
	
	downPressed: function (){
		return this.isDown(Game.keybindings['down']) || this.isDown(Game.keybindings['downALT'])
	}
});

Crafty.c('Tile', {
	init: function () {
		this.requires('Actor, Visual, SmartCollision');
	}
});

Crafty.c('SolidTile', {
	init: function () {
		this.requires('Tile, Solid');	
	}
});

Crafty.c('AutomatedMoving', {
	init: function () {
		this.requires('MovingActor, Delay');
		this.counter = 0;
		this.hyperactivity = 30;
		this.attention = 1000;
		this.timeToMove = 0;
	},
	
	getDirection: function (dirArray) {
		if (!dirArray){
			dirArray = this.defaultDirArray;	
		}
		this.direction = Math.floor(Math.random() * dirArray.length);
		this._movement.x = dirArray[this.direction][0];
		this._movement.y = dirArray[this.direction][1];
		this.delay(function () {
			this._movement.x = 0;
			this._movement.y = 0;
		}, this.attention);
	},
	
	defaultDirArray: [[1,0],[0,-1],[-1,0],[0,1]]
});

Crafty.c('Visual', {
	init: function () {
		this.requires('Actor');
		this.bind('MonsterTime', function () {
			this.visible = false;
		})  .bind('NormalTime', function () {
			this.visible = true;
		});
	}
});

Crafty.c('Person', {
	init: function () {
		this.requires('AutomatedMoving, Solid, Visual, Interactable, Food, spr_peopleBig');
		this.collision(new Crafty.polygon([5,10],[this._w-5,10],[this._w-5,this._h],[5,this._h]));
		this._movement = {x:0, y:0};
		
		this.viewRadius = 100;
		
		this.visualZone = Crafty.e("VisualZone");
		this.attach(this.visualZone);
		this.visualZone.attr({x:this._x+this._w/2,y:this._y+this._h/2});
		this.visualZone.setRadius(this.viewRadius);
		this.visualZone.playerInvisible = true;
		this.bind('EnterFrame', this._frame);
		this.bind('Alert', function(data) {
			if (data) {
				this.alert = data;
				this.alertPos = [data._x,data._y];
			}
		});
		this.bind('NormalTime', function() {
			if (!this.alwaysHostile){
				this.alert = false;
				this.removeComponent("Hostile");	
			}
		});
	},
	
	_frame: function () {
		this.counter = (this.counter + 1) % (this.hyperactivity+11);
		if (!this.alert){
			if (this.timeToMove === 0) {
				this.timeToMove = this.hyperactivity + Math.floor(Math.random()*10);
			} else if (this.counter === this.timeToMove) {
				this.getDirection();	
			}
		} else if (!this.dead) {
			this._alertMove();	
		}
		if (!this.conversation && !this.dead){
			this._actormove();
		}
		var door = this.sCollide('Door');
		if (door){
			door.interact();	
		}
	},
	
	violence: function () {
		if (!this.dead){
			this.dead = true;
			this.visualZone.destroy();
			this.visualZone = undefined;
			this.addComponent('Suspicious');
			this.removeComponent('Solid');
			this.removeComponent('Hostile');
			Crafty.trigger('CivilianDeath'); 
			//this.rotation = 90;
			this.alpha = .75;
			this._frame = function () {};
			this.bind('NormalTime', function() {
				this.sprite(2,0);
				this.rotation = 0;
				this.alpha = 1;
				this.removeComponent('Suspicious');
			});
		}
	}
});

Crafty.c('Friend', {
	init:function () {
		this.requires('Person');
		this.sprite(0,3);
		this.sheety = 3;
		this._movement = {x:0, y:0};
		this._alertMove = function() {
			this.addComponent("Hostile");
			if (3 > (Math.abs(this._x - this.alertPos[0]) + Math.abs(this._y - this.alertPos[1]))){
				this.search();
			} else {
				//Approach the space
				var anglex = this.alertPos[0] - this._x;
				var angley = this.alertPos[1] - this._y;
				var angle = Math.atan2(angley, anglex);
				this._movement.x = Math.cos(angle);
				this._movement.y = Math.sin(angle);
			}
		};
	},
	
	search: function() {
		this.attention = 3000;
		this.hyperactivity = 40;
		this.getDirection();
	}
});

Crafty.c('Gunner', {
	init: function () {
		this.requires('Person');
		this.sprite(0,4);
		this.sheety = 4;
		this.ySpeed = 2;
		this.xSpeed = 2;
		this._movement = {x:0, y:0};
		this._alertMove = function () {
			this.addComponent("Hostile");
			if (10 > (Math.abs(this._x - this.alertPos[0]) + Math.abs(this._y - this.alertPos[1]))){
				this.search();
			} else {
				//Approach the space
				var anglex = this.alertPos[0] - this._x;
				var angley = this.alertPos[1] - this._y;
				var angle = Math.atan2(angley, anglex);
				this._movement.x = Math.cos(angle);
				this._movement.y = Math.sin(angle);
				if (this.visualZone.sCollide('Player') && (this.counter % 20) === 0) {
					Crafty.audio.play("sfx_gunshot",1,.5);
					bullet = Crafty.e("Bullet").attr({x:this._x+this._w/2,y:this._y+this._h/2});
					bullet.setAngle(angle);
				}
			}
		};
	},
	
	search: function() {
		this.attention = 3000;
		this.hyperactivity = 40;
		this.getDirection();
	}
	
});

Crafty.c('Priest', {
	init: function() {
		this.requires('Person');
		this.sprite(0,5);
		this.sheety = 5;
		this._movement = {x:0, y:0};
		this._alertMove = function () {
			if (!this.has("Hostile")){
				this.addComponent("Hostile");
			}
			if (3 > (Math.abs(this._x - this.alertPos[0]) + Math.abs(this._y - this.alertPos[1]))){
				// Don't search,
			} else {
				//Approach the space
				var anglex = this.alertPos[0] - this._x;
				var angley = this.alertPos[1] - this._y;
				var angle = Math.atan2(angley, anglex);
				this._movement.x = Math.cos(angle);
				this._movement.y = Math.sin(angle);
			}
		};
	}
	
});

Crafty.c('Bullet', {
	init: function () {
		this.requires('SolidHitBox, Actor, Color, Delay, Hostile, SmartCollision');
		this.attr({h: 4, w:4});
		this.color('black');
		this._globalZ = 3;
		this.delay(function(){this.destroy()},2000);
		this.bind("EnterFrame", function() {
			if (this.sCollide('Solid')){
				this.delay(function() {
					this.destroy();	
				}, 100);
			}
		});
	},
	
	setAngle: function (angle) {
		this.angle = angle;
		this.bind('EnterFrame', function() {
			this.x += Math.cos(this.angle)* 10;
			this.y += Math.sin(this.angle)* 10;
		});
	}
});

Crafty.c('Yeller', {
	init: function () {
		this.requires('Person');	
		this.sprite(0,2);
		this.sheety = 4;
		this._movement = {x:0, y:0};
		this.ySpeed = 4;
		this.xSpeed = 4;
		this.bind('Alert', function() {
			if (this.yell){
				this.yell.destroy();
				this.yell = undefined;
			}
		})
		this._alertMove = function() {
			if (!this.yell){
				Crafty.audio.play("sfx_yell",1,.5);
				this.yell = Crafty.e('Delay');
				this.yell.delay(function () {this.destroy()}, 2000);
				this.hyperactivity = 5;
			}
			this.getDirection();
		}
	}
	
});

Crafty.c('Runner', {
	init: function () {
		this.requires('Person');
		this.sprite(0,1);
		this.sheety = 1;
		this._movement = {x:0, y:0};
		this.ySpeed = 7;
		this.xSpeed = 7;
		this.bind('Alert', function() {
			this.directions = undefined;
		});
		this._alertMove = function() {
			//Run away!
			if (!this.directions){
				var anglex = this.alertPos[0]-(Crafty.viewport._x) - this._x;
				var angley = this.alertPos[1]-(Crafty.viewport._y) - this._y;
				// Run any way but this way!
				var angle = Math.round(Math.atan2(angley, anglex)/(Math.PI/2))*4;
				this.directions = [[1,0],[0,-1],[-1,0],[0,1]];
				this.directions[angle] = [0,0];
			}
			if (this.counter === 0){
				this.getDirection(this.directions);	
			}
		}
	}
	
});

Crafty.c('Player', {
	init: function() {
		this.requires('PlayerActor, Mouse, Solid, spr_peopleBig');
		this.sheetx = 0;
		this.sheety = 0;
		this.counter = 0;
		
		this.maxHealth = 100;
		this.health = 100;
		this.healthBar = Crafty.e('Actor, Color, UI');
		this.healthBar.setCenteredPos(-214,-79,1,2);
		this.healthBar.defaulty = -79;
		this.healthBar.w = 102;
		this.healthBar.h = 69;
		this.healthBar.color('rgba(200,0,0,.75)');
		this.healthBar._globalZ = 10;
		
		this.maxHunger = 100;
		this.hunger = 100;
		this.hungerBar = Crafty.e('Actor, Color, UI');
		this.hungerBar.setCenteredPos(112,-79,1,2);
		this.hungerBar.defaulty = -79;
		this.hungerBar.w = 102;
		this.hungerBar.h = 69;
		this.hungerBar.color('rgba(200,100,0,.75)');
		this.hungerBar._globalZ = 10;
		
		this.collision(new Crafty.polygon([5,10],[this._w-5,10],[this._w-5,this._h],[5,this._h]));
		Crafty.addEvent(this, Crafty.stage.elem, 'click', function(e){
			if (!this.conversation && this.health > 0){
				this._interact(e);	
			}
		})
		this.bind('EnterFrame', this._frame);
		this.bind('ConversationStart', function(victim) {
			this.conversation = Crafty.e("Conversation");
			victim.conversation = this.conversation;
			this.conversation.victim = victim;
			this.conversation.player = this;
			this.conversationOptions = {goodbye: this.conversationDefaults["goodbye"],
									    deny: this.conversationDefaults["deny"],
									    worry: this.conversationDefaults["worry"]}
			if (victim.has("Friend") && Game.pop.threat === 100){
				this.conversationOptions["ending1"] = this.conversationDefaults["ending1"];	
			} else if (victim.has("Priest")) {
				this.conversationOptions["confess"] = this.conversationDefaults["confess"];	
			} else if (victim.has("Gunner")) {
				this.conversationOptions["threaten"] = this.conversationDefaults["threaten"];	
			}
		});
		this.bind('MonsterTime', function () {
			this.werewolf = true;
			this.addComponent('Suspicious');
			Game.time = "night";
			this.visualZone = Crafty.e("VisualZone");
			this.attach(this.visualZone);
			this.visualZone.attr({x:this._x+this._w/2,y:this._y+this._h/2});
			this.visualZone.setRadius(this.viewRadius);
			this.sprite(this.sheetx+1, this.sheety);
			this.ySpeed = this.werewolfSpeed;
			this.xSpeed = this.werewolfSpeed;
		});
		this.bind('NormalTime', function () {
			this.werewolf = false;
			this.removeComponent('Suspicious');
			Game.time = "day";
			this.sprite(this.sheetx, this.sheety);
			this.visualZone.destroy();
			this.ySpeed = this.normalSpeed;
			this.xSpeed = this.normalSpeed;
			this.oldRoofCollision = true;
		})
		this.bind('HealthChange', function (change) {
			this.health += change;
			if (this.health > this.maxHealth){
				this.health = this.maxHealth;	
			}
			if (this.health <= 0) {
				if (this.confessed){
					Crafty.trigger('GameEnd', Game.confessionText);	
				} else {
					Crafty.trigger('GameEnd', Game.deathText);	
				}
				return;
			}
			this.healthBar.h = (this.health*69)/(this.maxHealth);
			this.healthBar.setCenteredPos(-214,this.healthBar.defaulty + (69 - this.healthBar._h),1,2);
		})
		this.bind('HungerChange', function (change) {
			this.hunger += change;
			if (this.hunger > this.maxHunger){
				this.hunger = this.maxHunger;	
			} else if (this.hunger < 0) {
				this.hunger = 0;	
			}
			this.hungerBar.h = (this.hunger*69)/(this.maxHunger);
			this.hungerBar.setCenteredPos(112,this.hungerBar.defaulty + (69 - this.hungerBar._h),1,2);
		})
		this.bind('KeyDown', function(){
			if(this.isDown("M")){
				Crafty.audio.toggleMute();	
			}
		});
		
	},
	
	_frame: function() {
		console.log(Game.pop.threat);
		this.counter = (this.counter + 1) % 41;
		if (this.counter === 40){
			this.trigger('HungerChange', -1);
			if (this.hunger <= 0) {
				this.trigger('HealthChange', -1);
			}
		}
		if (!this.conversation){
			this._actormove();
			// Move the screen when the player hits the edge of the screen
			if (this.x > ((-Crafty.viewport._x + (Crafty.viewport._width / Crafty.viewport._scale))-48)){
				Crafty.viewport.x -= Math.floor(Game.width()/2);
				Crafty.trigger('MoveUI', ["x", Math.floor(Game.width()/2)]);
			} else if (this.x < (-Crafty.viewport._x + 48)) {
				Crafty.viewport.x += Math.floor(Game.width()/2);	
				Crafty.trigger('MoveUI', ["x", -Math.floor(Game.width()/2)]);
			} 
			if (this.y > ((-Crafty.viewport._y + (Crafty.viewport._height / Crafty.viewport._scale))-140)) {
				Crafty.viewport.y -= Math.floor(Game.height()/2);
				Crafty.trigger('MoveUI', ["y", Math.floor(Game.height()/2)]);
			} else if (this.y < (-Crafty.viewport._y + 48)) {
				Crafty.viewport.y += Math.floor(Game.height()/2);
				Crafty.trigger('MoveUI', ["y", -Math.floor(Game.height()/2)]);
			}
		}
		// When you first collide with a roof, tell all the rooves to turn invisible
		if (!this.oldRoofCollision && this.sCollide("Roof")){
			this.oldRoofCollision = true;
			Crafty.trigger('RoovesDissapear');
		// When you had collided with a roof and are no longer, make them come back.
		} else if (this.oldRoofCollision && !this.sCollide("Roof")) {
			this.oldRoofCollision = false;
			Crafty.trigger('RoovesAppear');
		}
		var hostile = this.sCollide('Hostile');
		if (hostile){
			if (hostile.has("Bullet")){
				hostile.destroy();
				this.trigger('HealthChange', -30);
			} else if (hostile.has("Priest")){
				this.trigger('HealthChange', -4);
			} else {
				this.trigger('HealthChange', -2);
				this.confessed = false;
			}
		}
	},
	
	_interact: function(data) {
		var anglex = data.clientX-(Crafty.viewport._x) - this._x;
		var angley = data.clientY-(Crafty.viewport._y) - this._y;
		var angle = Math.atan2(angley, anglex);
		var target = Crafty.e('Interactor')
		if (this.werewolf) {
			Crafty.audio.play("sfx_bite",1,.5);
			target.requires('spr_peopleBig');
			target.sprite(2,1);
			target.attr({x:this._x + this._w/2 - 9 + Math.cos(angle)*15*Game.scale,
					 y:this._y + this._h/2 - 9 + Math.sin(angle)*15*Game.scale});
			var victim = target.sCollide('Interactable')
			//target.addComponent('SolidHitBox');
			target._flipX = (anglex < 0); 
			//target.rotation = (180/Math.PI)*(angle+(Math.PI/4));
			// Attack them
			if (victim){
				if (victim.has('Food')){
					this.trigger('HungerChange', 50);
					this.trigger('HealthChange', 1);
				}
				victim.violence();
			}
		} else {
			target.attr({x:this._x + this._w/2 + Math.cos(angle)*15*Game.scale,
					 y:this._y + this._h/2 + Math.sin(angle)*15*Game.scale});
			var victim = target.sCollide('Interactable')
			target.addComponent('SolidHitBox');
			if (victim){
				// If there's a person there, start a dialogue scene with them
				if (victim.has('Person') && !victim.has("Hostile") && !victim.dead){
					Crafty.trigger('ConversationStart', victim);
				// Otherwise do whatever rudimentary thing the thing does
				} else if (!victim.has("Hostile")) {
					victim.interact();	
				}
			}
		}
	},
	
	_movement: {
		x: 0,
		y: 0
	},
	
	conversationDefaults: {
		goodbye: "So long.",
		deny: "You know that the werewolf isn't me, right?",
		worry: "Watch out tonight, I don't want you getting hurt.",
		threaten: "Are you gonna be using that gun on me, pansy?",
		confess: "Mother, I need to say this. I am the monster.",
		ending1: "Look, yes, I'm the monster, but I'm not going to hurt you. Trust me.",
		ending2: "Let's get out of here. Just you and me. We can live on our own."
	},
	
	viewRadius: 125,
	
	werewolfSpeed: 8,
	
	normalSpeed: 5,
	
	werewolf: false
});

Crafty.c('VisualZone', {
	init: function () {
		this.requires('2D, SmartCollision');
		this.visible = false;
		this.hitTiles = [];
		this.one('EnterFrame', this._updateF);
		this.bind('Move', this._updateF);
	},
	_updateF: function(){
		if (this.playerInvisible){
			if (!this._parent.dead){
				if (this._parent.alwaysHostile){
					var suspiciousThing = this.sCollide("Player");
				} else {
					var suspiciousThing = this.sCollide("Suspicious");
				}
				if (suspiciousThing && suspiciousThing !== this._parent) {
					this._parent.trigger('Alert', suspiciousThing);
				}
			}
			return;	
		} else {
			var tiles = this.hit("Visual")
			if (tiles){
				this.hitTiles.forEach(function(tile){
					if (tiles.indexOf(tile) === -1){
						tile.obj.visible = false;	
					}
				}, this);
				this.hitTiles = tiles;
				this.hitTiles.forEach(function(tile){
					if (!tile.obj.visible && (!tile.obj.has("Roof") || (this._parent && !this._parent.oldRoofCollision))) {
						tile.obj.visible = true;	
					}
				}, this);
			}
			if (this._parent.has('Player')){
				var adjacentZones = this.hit("VisualZone");
				if (adjacentZones){
					adjacentZones.forEach(function(zone) {
						zone.obj._updateF();
					});
				}
			}
		}
	},
	
	setRadius: function(radius){
		this.collision(new Crafty.circle(0,0,radius));
	}
	
});

Crafty.c('Interactor', {
	init: function () {
		this.requires('SmartCollision,Delay');
		this.attr({w:4,h:4});
		this.delay(function(){this.destroy()},100);
	}
});

Crafty.c('UI', {
	init: function() {
		this.requires('2D, Canvas');
		this.bind('MoveUI', function(data) {
			if (data[0] === "x"){
				this.x += data[1];	
			} else {
				this.y += data[1];	
			}
		});
		this.xSide = 0;
		this.ySide = 0;
		this.screenXOffset = 0;
		this.screenYOffset = 0;
		this.bind('ViewportResize', function () {
				this._bindScreenPos();
			})
			.bind('ViewportZoomed', function (){
				this._bindScreenPos();
			});
		this._globalZ = Game.uiGlobalZ();
	},
	
	_bindScreenPos: function() {
		if (this.screenBound){
			this.screenx = this.screenXOffset + (Game.width()/2 * this.xSide);
			this.screeny = this.screenYOffset + (Game.height()/2 * this.ySide);
		}
		this._moveWithViewport();
	},
	
	_moveWithViewport: function() {
		this.x = this.screenx + -1*(Crafty.viewport.x);
		this.y = this.screeny + -1*(Crafty.viewport.y);	
	},
	
	setScreenPos: function(x,y){
		this.screenx = x;
		this.screeny = y;
		this._moveWithViewport();
	},
	
	setCenteredPos: function(x,y,xSide,ySide){
		this.screenBound = true;
		this.screenXOffset = x;
		this.screenYOffset = y;
		this.xSide = xSide;
		this.ySide = ySide;
		this._bindScreenPos();
	}
});

Crafty.c('Timer', {
	init: function() {
		this.requires('Actor, Text, Delay, UI');
		this.hour = 12;
		this.colon = ":";
		this.minute = 0;
		this.timeSpeed = 100;
		this.textColor('rgb(255,255,255)');
		this.textFont({family: 'Verdana', size: '18px'});
		this.text("17:00");
		this.delay(function() {
			if (!this.paused){
				this.minute = (this.minute + 1) % 60;
				if (this.minute === 0) {
					this.hour = (this.hour + 1) % 24;
					if (this.hour === 18){
						Crafty.trigger("MonsterTime");	
					} else if (this.hour === 6) {
						Crafty.trigger("NormalTime");	
					}
				}
			}
			this.colon = ":";
			if (this.minute < 10){
					this.colon = ':0';
				}
			this.textColor('white');
			this.text(this.hour.toString() + this.colon + this.minute.toString());
		}, this.timeSpeed, -1);
		this.bind('ConversationStart', function() {
			this.paused = true;
		})  .bind('ConversationStop', function() {
			this.paused = false;	
		});
		this._globalZ = 100;
	}
});

Crafty.c('Roof', {
	init: function() {
		this.requires('Tile, Delay, SmartCollision');
		this.bind('RoovesDissapear', function () {
			if (this.visible){
				this.visible = false;
			}
		})  .bind('RoovesAppear', function () {
			if (!this.visible && (Game.time === "day" || this.sCollide('VisualZone'))){
				this.visible = true;
			}
		})
	}
})

Crafty.c('Conversation', {
	init: function () {
		this.requires('Actor, spr_dialogueBox, UI, Delay');
		this.setCenteredPos(-320,0,1,0);
		this._globalZ = 10;
		this.textBody = Crafty.e('Actor, Text, UI');
		this.textBody.textColor("white");
		this.textBody.textFont({family: 'Verdana', size: '18px'});
		this.textBody.setCenteredPos(-310,8,1,0);
		this.textBody._globalZ = 11;
		this.attach(this.textBody);
		this.one('EnterFrame', function(){
			this.conversationStep("hello");	
		});
	},
	
	
	
	conversationStep: function (previous) {
		// Destroy old buttons
		buttons = Crafty("ConversationButton");
		if (buttons){
			if (Array.isArray(buttons)){
				for (var i = 0; i < buttons.length; i++){
					buttons[i].destroy();	
				}
			} else {
				buttons.destroy();
			}
		}
		if (previous === "goodbye"){	
			this.delay(function () {
				Crafty.trigger('ConversationStop');
				this.player.conversation = undefined;
				this.victim.conversation = undefined;
				this.destroy();
			}, 1000);
		} else if (previous === "ending1") {
			this.player.conversationOptions["ending2"] = this.player.conversationDefaults["ending2"];
		} else if (previous === "ending2") {
			Crafty.trigger('ConversationStop');
			Crafty("ConversationButton").destroy();
			Crafty.trigger("GameEnd", Game.friendshipText);
		} else if (previous === "threaten" && Game.pop.threat > 59) {
			this.victim.trigger("Alert", this.player);
			this.victim.alwaysHostile = true;
			this.delay(function () {
				Crafty.trigger('ConversationStop');
				this.player.conversation = undefined;
				this.victim.conversation = undefined;
				this.destroy();
			}, 1000);	
		} else if (previous === "confess") {
			this.victim.trigger("Alert", this.player);
			this.victim.alwaysHostile = true;
			this.victim.addComponent('Hostile');
			this.player.confessed = true;
			this.delay(function () {
				Crafty.trigger('ConversationStop');
				this.player.conversation = undefined;
				this.victim.conversation = undefined;
				this.destroy();
			}, 2000);	
		}
		
		
		this.textBody.text(Game.pop.dialogue[this.victim.sheety][Game.pop.threat][previous]);
		this.textBody.draw();
		
		var buttonx = -318;
		var buttony = 82;
		for (var key in this.player.conversationOptions){
			if (this.player.conversationOptions.hasOwnProperty(key)){
				button = Crafty.e("ConversationButton");
				button.setCenteredPos(buttonx, buttony,1,0);
				button.val = key;
				button.assignText(this.player.conversationOptions[key]);
				this.attach(button);
				buttony += 20;
			}
		}
	}
});

Crafty.c('Button', {
	init: function (){
		this.requires('Actor, Mouse, UI');
		this.alpha = .75;
		this.bind('MouseOver', function() {
			this.alpha = 1;
		})  .bind('MouseOut', function() {
			this.alpha = .75;
		})  .bind('MouseDown', function() {
			this.alpha = .5;	
		}); 
	}
});

Crafty.c('StartButton', {
	init: function (){
		this.requires('Button, spr_startButton');
		this.one('Click', function () {
			Crafty.trigger('StartGamePrep');
		});
	}
});

Crafty.c('ConversationButton', {
	init: function(){
		this.textelement = Crafty.e('Text, UI');
		this.requires('Button, spr_textButton');
		this.attach(this.textelement);
		weirdThingThatLetsOtherThingsHaveColor = Crafty.e('Actor, Color').attr({x:-5,y:-5})
			  .color("white");
		this.textelement.textColor('white');
		this.textelement.textFont({family: 'Verdana', size: '16px'});
		this._globalZ = 12;
		this.textelement._globalZ = 13;
		this.bind('EnterFrame', function() {
			// This actually places things in the right order.
			// It -shouldn't- need to happen, but it's dumb and a wierd bug with crafty and text drawing
			// slash anything that isn't a sprite.
			Crafty.viewport.x = Crafty.viewport._x;	
		})
		this.one('EnterFrame', function() {
			this.collision(Crafty.polygon([this._x,this._y],[this._x,this._y+this._h],
						 [this._x+this._w,this._y+this._h],
						 [this._x+this._w,this._y]));
		});
		this.bind('Click', function() {
			this._parent.conversationStep(this.val);
		});
		this.bind("EnterFrame", function() {
			this.textelement.textColor('rgba(255,255,0,1)');
		});	
	},
	
	assignText: function (string){
		this.textelement.attr({x:this._x+5, y:this._y});
		this.textelement.textColor('white');
		this.texttoset = string;
		this.textelement.text(string);
	}
});

Crafty.c('Door', {
	init: function (){
		this.requires('SolidTile, Delay, SmartCollision, Interactable');
	},
	
	interact: function () {
		Crafty.audio.play("sfx_bonk",1,.5);
		this.removeComponent('Solid');
		this.sprite(2,0);
		this.delay(this.showUp, 2000);
		
	},
	
	violence: function () {
		this.removeComponent('Solid');
		this.sprite(2,0);
		this.delay(this.showUp, 20000);
	},
	
	showUp: function () {
		if (this.sCollide('Player')){
			this.delay(this.showUp, 2000);
			return;
		}
		this.sprite(this.sheetx,this.sheety);
		this.addComponent('Solid');	
	}
});

Crafty.c('Window', {
	init: function () {
		this.requires('SolidTile, SmartCollision, Delay, Interactable');	
	},
	
	interact: function () {
		Crafty.audio.play("sfx_bonk",1,.5);
	},
	
	violence: function () {
		Crafty.audio.play("sfx_glass",1,.5);
		this.removeComponent('Solid');
		this.sprite(0,2);
		this.addComponent('Suspicious');
		this.bind("NormalTime", this.showUp);
	},
	
	showUp: function () {
		if (this.sCollide('MovingActor')){
			this.delay(this.showUp, 2000);
			return;
		}
		this.removeComponent('Suspicious');
		this.sprite(this.sheetx,this.sheety);
		this.addComponent('Solid');
	}
});

Crafty.c('Torch', {
	init: function() {
		this.requires('SolidTile, Interactable');
		this.viewRadius = 24;
		this.oldRoofCollision = true;
		this.bind("MonsterTime", function () {
			if (!this.hasVisualZoneDestroyed){
				this.visualZone = Crafty.e("VisualZone");
				this.attach(this.visualZone);
				this.visualZone.attr({x:this._x+this._w/2,y:this._y+this._h/2});
				this.visualZone.setRadius(this.viewRadius);
				this.visualZone.torch = true;
			}
		})  .bind('NormalTime', function () {
			this.visualZone.destroy();
		});
	},
	
	interact: function() {
		Crafty.audio.play("sfx_bonk",1,.5);
	},
	
	violence: function() {
		Crafty.audio.play("sfx_glass",1,.5);
		this.sprite(1,1);
		this.hasVisualZoneDestroyed = true;
		this.visualZone.destroy();
	}
});

Crafty.c('Tree', {
	init: function() {
		this.requires('SolidTile, Interactable');
	},
	
	interact: function() {
		Crafty.audio.play('sfx_bonk',1,.5);
		//wood knocking	
	},
	
	violence: function() {
		this.removeComponent('Solid')
		this.removeComponent('Interactable');
		this.sprite(this.sheetx+1,this.sheety);	
	}
	
});

Crafty.c('Sheep', {
	init: function (){
		this.requires('spr_peopleBig, AutomatedMoving, Visual, Solid, Interactable, Food');
		this._movement = {
			x: 0,
			y: 0
		},
		this.sprite(1,1);
		this.hyperactivity = 100 + Math.floor(Math.random()*10);
		this.attention = 500 + Math.floor(Math.random()*100);
		this.ySpeed = 2;
		this.xSpeed = 2;
		this.bind('EnterFrame', function () {
			this.counter = (this.counter + 1) % (this.hyperactivity+11);
			if (this.timeToMove === 0) {
				this.timeToMove = this.hyperactivity + Math.floor(Math.random()*10);
			} else if (this.counter === this.timeToMove) {
				this.getDirection();	
			}
			this._actormove();
		});
	},
	
	interact: function() {
		Crafty.audio.play("sfx_baa",1,.5);
	},
	
	violence: function() {
		Crafty.audio.play("sfx_baa",1,.5);
		this.destroy();	
	}
});

Crafty.c('Fence', {
	init: function(){
		this.requires('SolidTile, Interactable');	
	},
	
	interact: function () {
		//wood knocking sound	
	},
	
	violence: function () {
		this.removeComponent('Solid');
		this.removeComponent('Interactable');
		this.addComponent('Suspicious');
		this.sprite(1,5);
		this.bind('NormalTime', function() {
			this.sprite(0,0);
			this.removeComponent('Suspicious');
		});
	}
	
});