The Wolf Comes Out at 18:00 is an HTML5/Javascript game made for Ludum Dare 33 with Crafty.js.

Update 1 after deadline fixes a crash bug on bullets being fired. Also includes compatability changes for older versions of IE (being, almost exclusively, removing trailing commas from objects)

Second update after deadline causes interacting with gravestones to not cause a crash.